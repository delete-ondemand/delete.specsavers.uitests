﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using System.Linq;

namespace IntegrationTests

{
	
	class MediaPlanPage : BaseIntegrationTest
	{
		private BrowserType _browserType;
		public MediaPlanPage(BrowserType browserType) : base(browserType)
		{
			_browserType = browserType;
			AddMediaButton = By.CssSelector("div.bpt__head>a:first-of-type>span");
			MediaTypeDropdown = By.CssSelector("#Type_chzn>a>span");			
			SubCategoryDropdown = By.Id("SubCategoryId_chzn");
			QuantityInput = By.Id("Quantity");
			SaveMediaButton = By.Id("save-media");
			MediasWithoutInsertions = By.CssSelector("div.bpt__list>div[data-media-id]");
			PublicationInput = By.CssSelector("input[data-modelid=\"PublicationId\"]");
			PublicationResult = By.CssSelector("div.ac_results");
		}

		internal By MediaTypeValue(int mediaType)
		{
			return By.Id("Type_chzn_o_" + mediaType);
		}

		internal By SubCategoryValue(int subCategory)
		{
			return By.Id("SubCategoryId_chzn_o_" + subCategory);
		}

		internal By AddInsertionCell(int mediaId, string date)
		{
			return By.CssSelector($"a[href^=\"/Insertions/Add?mediaId={mediaId}&insertionDate={date}\"]");
		}
		internal By InsertionWithStatusCell(int mediaId)
		{
			return By.CssSelector($"td.bvt-status a[data-media-id=\"{mediaId}\"]");
		}

		internal By RemoveMediabyId(int mediaId)
		{
			return By.CssSelector($"a.remove-media[href^=\"/Media/Remove/{mediaId}\"]");
		}
		public bool RemoveMedia(int mediaId)
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(RemoveMediabyId(mediaId)).Displayed);
			Driver.FindElement(RemoveMediabyId(mediaId)).Click();
			bool isRemoved = false;

			try
			{
				IAlert alert = Driver.SwitchTo().Alert();
				isRemoved = true;
				alert.Accept();
			}
			catch (NoAlertPresentException ex)
			{
				ex.Message.ToString();
			}
			return isRemoved;
		}

		public int GetLastMediaId()
		{
			List<int> mediaIdList = new List<int>();
			var mediaIdElements = Driver.FindElements(MediasWithoutInsertions);
			int mediaCount = Driver.FindElements(MediasWithoutInsertions).Count;
			for (int i = 0; i < mediaCount; i++)
			{				
				mediaIdList.Add(Int32.Parse(mediaIdElements[i].GetAttribute("data-media-id")));
			}
			return mediaIdList.Max();

		}

		public string InsertionDate()
		{
			var insertionDate = DateTime.Today;

			var dayOfWeek = (int)insertionDate.DayOfWeek;
			var delta = 1 - dayOfWeek;
			if (delta == 0)
				insertionDate = DateTime.Today.AddDays(7);
			if (delta > 0)
				insertionDate = DateTime.Today.AddDays(1);
			else
				insertionDate = DateTime.Today.AddDays(7 + delta);

			var day = insertionDate.Day.ToString();
			if (day.Length == 1)
				day = $"0{day}";

			var month = insertionDate.Month.ToString();
			if (month.Length == 1)
				month = $"0{month}";

			return $"{month}%2F{day}";
		}
		public int AddNewMedia(int mediaType, int subCategory)
		{
			Driver.FindElement(AddMediaButton).Click();
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(MediaTypeDropdown).Displayed);
			Driver.FindElement(MediaTypeDropdown).Click();
			Driver.FindElement(MediaTypeValue(mediaType)).Click();
			Driver.FindElement(SubCategoryDropdown).Click();
			Driver.FindElement(SubCategoryValue(subCategory)).Click();
			if (mediaType == 16)
			{
				Driver.FindElement(PublicationInput).SendKeys("New Publication");
				isDisplayed = Wait.Until(Driver => Driver.FindElement(PublicationResult).Displayed);
				Driver.FindElement(PublicationResult).Click();
			}
			if (mediaType == 1)
				Driver.FindElement(QuantityInput).SendKeys("4");
			Driver.FindElement(SaveMediaButton).Click();
			WaitUntilDocumentIsReady();
			var mediaId = GetLastMediaId();
			return mediaId;
		}

		//Open Create insertion popup
		public InsertionOverlay AddInsertion(int mediaId)
		{
			var insertionDate = InsertionDate();
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(AddInsertionCell(mediaId, insertionDate)).Displayed);
			Driver.FindElement(AddInsertionCell(mediaId, insertionDate)).Click();
			return new InsertionOverlay(_browserType);
		}

		public InsertionOverlay OpenInsertionDetails(int mediaId)
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(InsertionWithStatusCell(mediaId)).Displayed);
			Driver.FindElement(InsertionWithStatusCell(mediaId)).Click();
			var insertionOverlay = new InsertionOverlay(_browserType);
			isDisplayed = Wait.Until(Driver => Driver.FindElement(insertionOverlay.InsertionStatus).Displayed);
			return insertionOverlay;

		}
		public int GetInsertionId(int mediaId)
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(InsertionWithStatusCell(mediaId)).Displayed);
			var el = Driver.FindElement(InsertionWithStatusCell(mediaId));
			var insertionId = Int32.Parse(el.GetAttribute("data-insertion-id"));
			return insertionId;
		}

		

		public By AddMediaButton { get; set; }
		public By AddInsertionButton { get; set; }
		public By MediaDetailsButton { get; set; }
		public By InsertionDetailButton { get; set; }
		public By MediaTypeDropdown { get; set; }
		public By SubCategoryDropdown { get; set; }
		public By MediaCostInput { get; set; }
		public By QuantityInput { get; set; }
		public By SaveMediaButton { get; set; }
		public By MediasWithoutInsertions { get; set; }
		public By PublicationInput { get; set;  }
		public By PublicationResult { get; set; }





	}
}
