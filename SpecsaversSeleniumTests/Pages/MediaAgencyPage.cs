﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace IntegrationTests
{
	class MediaAgencyPage : BaseIntegrationTest
	{
		private BrowserType _browserType;
		public MediaAgencyPage(BrowserType browserType) : base(browserType)
		{
			_browserType = browserType;
			//StoresDropdown = By.Id("StoreId_chzn");
			//StoreValue = By.CssSelector("#StoreId_chzn>a>span");
			//StoreValue = By.Id("StoreId_chzn_o_886");
			//ApplyFilerButton = By.Id("apply-filter");
			
			QueriesTab = By.CssSelector("a[data-tab-name=\"AgencyQueriesList\"]");
			QueriesTabContext = By.CssSelector("form#FilterForm[data-ajax-url=\"/Insertions/AgencyQueriesList\"]");
			H1Label = By.XPath("//h1[contains(text()[2], \"bookings with outstanding queries\")]");
			
			
		}
		
		/*internal By QueryStatus(int insertionId)
		{
			//return $"$('tbody tr a[href^=\"/Insertions/Details/{insertionId}\"]').closest('td').next()";
			return By.CssSelector($"tbody tr a[href^=\"/Insertions/Details/{insertionId}\"]");
		}*/

		
		public InsertionOverlay OpenInsertionOverlay(int insertionId)
		{
			//var isDisplayed = Wait.Until(Driver => Driver.FindElement(TableSingleRow).Displayed);
			Driver.FindElement(InsertionToQuery(insertionId)).Click();
			//var insertionOverlay = new InsertionOverlay(_browserType);
			//insertionOverlay.QueryInsertionByMediaAgency(insertionId);
			return new InsertionOverlay(_browserType);
			
			/*IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
			System.Threading.Thread.Sleep(1000);
			//string jscode = "$('table.b-results>tbody tr a.js-show-insertions-link').click()";
			string jscode = $"$('table.b-results>tbody tr a[href^=\"/Insertions/Details/{insertionId}\"]').click()";
			var jscodeResult = js.ExecuteScript(jscode);*/

		}


		public void ProceedToQueriesTab()
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(QueriesTab).Displayed);
			Driver.FindElement(QueriesTab).Click();
			isDisplayed = Wait.Until(Driver => Driver.FindElement(H1Label).Displayed);
			ShowAlert("Queries tab update completed");

		}
		public string CheckAgencyQueryStatus(int insertionId)
		{
			
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(QueriesTabContext).Displayed);
			return Driver.FindElement(QueryStatus).Text;

			// TODO: Find out how to build a selector that finds next element by a spesific condition  

			/*IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
			//string jscode = QueryStatus(insertionId);
			var element = Driver.FindElement(QueryStatus(insertionId));
			var element1 = ((RemoteWebElement)js.ExecuteScript("return arguments[0]", element)).Text;
			var element2 = js.ExecuteScript("return $(arguments[0]).text()", element); works correctly
			var element2 = js.ExecuteScript("return $(arguments[0]).closest('td').next()", element); - throws too long chain exception

			//var jscodeResult = (RemoteWebElement)js.ExecuteScript("return $(arguments[0].closest('td').next())", element1);*/

			//return "";//(String)jscodeResult;
		}

		public string CheckPlannerQueryStatus(int insertionId)
		{
			return Driver.FindElement(QueryStatus).Text;
		}


		//public By StoresDropdown { get; set; }
		//public By StoreValue { get; set; }
		//public By ApplyFilerButton { get; set; }

		public By QueriesTab{ get; set; }
		public By QueriesTabContext{ get; set; }
		public By H1Label { get; set; }


	}
	
}
