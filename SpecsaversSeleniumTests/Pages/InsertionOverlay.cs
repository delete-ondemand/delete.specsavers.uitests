﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using SeleniumExtras;

namespace IntegrationTests
{
	class InsertionOverlay : BaseIntegrationTest
	{
		public InsertionOverlay(BrowserType browserType) : base(browserType)
		{

			CampaignCodeDropdown = By.Id("CampaignCodeId_chzn");
			CampaignCodesList = By.CssSelector("ul.chzn-results>li[id^=\"CampaignCodeId\"]");
			SubmitButton = By.Id("set-status-btn");
			InsertionStatus = By.CssSelector("div.bl__status>span");
			RemoveInsertionButton = By.CssSelector("input.btn-silver[value=\"Remove Insertion\"]");
			CancelInsertionButton = By.CssSelector("input.btn-silver[value=\"Cancel Insertion\"]");
			QueryInsertionButton = By.CssSelector("input.btn-silver[value=\"Query Booking\"]");
			QueryCommentInput = By.CssSelector("div.b-add-comment>textarea#Comment");
			SubmitQueryToPlannerButton = By.CssSelector("div#note-container>input[value=\"Query Planner\"]");
			OverlayIsOpened = By.CssSelector("div.b-overlay");
			PlannerAddCommentButton= By.CssSelector("input.btn-silver[value=\"Add comment\"]");
			SubmitCommentButton = By.CssSelector("div#note-container>input[value=\"Submit comment\"]");
			SubmitResolvedQueryButton = By.CssSelector("input.b-green-btn[value=\"Submit\"]");
			SetInsertionAsResolvedValue = By.XPath("//li[contains(text(), \"Set as resolved\")]");
			//SetInsertionAsResolvedValue = By.CssSelector("select[role=\"query-status-selector\"]>option:nth-child(2)");
			SendInsertionToPlannerValue = By.XPath("//li[contains(text(), \"Send to Planner\")]");
			ActionsDropdown = By.CssSelector("div#btn-container div.chzn-container>a.chzn-single>span");

		}

		internal By AgencyBookingRequired(string value)
		{
			return By.CssSelector($"div.editor-field>input[name=\"BookingRequired\"][value=\"{value}\"]");
		}
		internal By ProductiongRequired(string value)
		{
			return By.CssSelector($"div.editor-field>input[name=\"ProductionRequired\"][value=\"{value}\"]");
		}
		internal By StoreApprovalRequired(string value)
		{
			return By.CssSelector($"div.editor-field>input[name=\"IsStoreApprovalRequired\"][value=\"{value}\"]");
		}
		internal By CampaignCodeValue(int index)
		{
			return By.Id($"CampaignCodeId_chzn_o_{index}");
		}

		public void SelectCampaignCode()
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(CampaignCodeDropdown).Displayed);
			Driver.FindElement(CampaignCodeDropdown).Click();
			isDisplayed = Wait.Until(Driver => Driver.FindElement(CampaignCodesList).Displayed);
			int campaignCodesCount = Driver.FindElements(CampaignCodesList).Count;
			Driver.FindElement(CampaignCodeValue(Random(campaignCodesCount))).Click();

				
		}

		public bool RemoveInsertion()
		{
			Driver.FindElement(RemoveInsertionButton).Click();
			bool isRemoved = false;

			try
			{
				IAlert alert = Driver.SwitchTo().Alert();
				isRemoved = true;
				alert.Accept();
			}
			catch (NoAlertPresentException ex)
			{
				ex.Message.ToString();
			}
			ShowAlert("Insertion was successfully removed");
			return isRemoved;
		}
		public void CreateNewInsertion()
		{
			Driver.FindElement(SubmitButton).Click();
			WaitForElementToDisappear(OverlayIsOpened);
			ShowAlert("Insertion has been successfully created");
		}

		public bool CancelInsertion()
		{
			Driver.FindElement(CancelInsertionButton).Click();
			bool isRemoved = false;

			try
			{
				IAlert alert = Driver.SwitchTo().Alert();
				isRemoved = true;
				alert.Accept();
			}
			catch (NoAlertPresentException ex)
			{
				ex.Message.ToString();
			}
			return isRemoved;
		}

		public void WaitForinsertionOverlayToClose()
		{
			Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(OverlayIsOpened));
			ShowAlert("Insertion overlay has been closed");
			/*var element = Driver.FindElement(QueryStatus(insertionId));
			var element1 = ((RemoteWebElement)js.ExecuteScript("return arguments[0]", element)).Text;
			var element2 = js.ExecuteScript("return $(arguments[0]).text()", element); works correctly
			var element2 = js.ExecuteScript("return $(arguments[0]).closest('td').next()", element); - throws too long chain exception

			//var jscodeResult = (RemoteWebElement)js.ExecuteScript("return $(arguments[0].closest('td').next())", element1);*/
		}

		public void QueryInsertionByMediaAgency(int insertionId)
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(QueryInsertionButton).Displayed);
			Driver.FindElement(QueryInsertionButton).Click();
			isDisplayed = Wait.Until(Driver => Driver.FindElement(QueryCommentInput).Displayed);
			Driver.FindElement(QueryCommentInput).SendKeys($"Insertion {insertionId} queried by Media Agency");
			Driver.FindElement(SubmitQueryToPlannerButton).Click();
			WaitForinsertionOverlayToClose();
		}

		public void PlannerReplyQuery(int insertionId)
		{
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(PlannerAddCommentButton).Displayed);
			Driver.FindElement(PlannerAddCommentButton).Click();
			isDisplayed = Wait.Until(Driver => Driver.FindElement(QueryCommentInput).Displayed);
			Driver.FindElement(QueryCommentInput).SendKeys($"Planner replied to Media Agency query in insertion {insertionId}");
			Driver.FindElement(SubmitCommentButton).Click();
			WaitForinsertionOverlayToClose();
			Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(InsertionToQuery(insertionId)));
			ShowAlert("Insertion removed from table");

		}



		public void Set_Production_StoreApproval_AgencyBooking(string productiongRequired, string storeApprovalRequired, string agencyBookingRequired)
		{
			Driver.FindElement(ProductiongRequired(productiongRequired)).Click();
			Driver.FindElement(StoreApprovalRequired(storeApprovalRequired)).Click();
			Driver.FindElement(AgencyBookingRequired(agencyBookingRequired)).Click();

		}

		public void MediaAgencySetInsertionAsResolved(int insertionId)
		{

			var isDisplayed = Wait.Until(Driver => Driver.FindElement(ActionsDropdown).Displayed);
			Driver.FindElement(ActionsDropdown).Click();
			isDisplayed = Wait.Until(Driver => Driver.FindElement(SetInsertionAsResolvedValue).Displayed);
			Driver.FindElement(SetInsertionAsResolvedValue).Click();
			//IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
			//js.ExecuteScript("arguments[0].setAttribute('class','active-result result-selected')", element);
			ShowAlert("Option selected");
			Driver.FindElement(SubmitResolvedQueryButton).Click();
			WaitForinsertionOverlayToClose();
			//Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(InsertionToQuery(insertionId)));
			WaitForElementToDisappear(InsertionToQuery(insertionId));
			ShowAlert("Insertion removed from table");
		}

		public void WaitForElementToDisappear(By locator)
		{
			Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(locator));

		}
		public By SubmitButton { get; set; }
		public By CampaignCodeDropdown { get; set; }
		public By CampaignCodesList { get; set; }
		public By InsertionStatus { get; set; }
		public By RemoveInsertionButton { get; set; }
		public By CancelInsertionButton { get; set; }
		public By QueryInsertionButton { get; set; }
		public By QueryCommentInput { get; set; }
		public By SubmitQueryToPlannerButton { get; set; }
		public By OverlayIsOpened { get; set; }
		public By PlannerAddCommentButton { get; set; }
		public By SubmitCommentButton { get; set; }
		public By SetInsertionAsResolvedValue { get; set; }
		public By SubmitResolvedQueryButton { get; set; }
		public By SendInsertionToPlannerValue { get; set; }
		public By ActionsDropdown { get; set; }
	}
}
