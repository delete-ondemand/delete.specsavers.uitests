﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegrationTests
{
	class LoginPage : BaseIntegrationTest
	{
		public LoginPage(BrowserType browserType) : base(browserType)
		{

			Email = By.Id("Email");
			Password = By.Id("Password");
			LoginButton = By.CssSelector("input.btn-silver");
			LoggedInUser = By.CssSelector("li.b-logged-user");
		}

		public void LoginAsUser(string user)
		{
			GoToPage(HtmlConsts.Host);
			var e = Driver.FindElement(Email); 
			e.SendKeys(user);
			Driver.FindElement(Password).SendKeys(HtmlConsts.Password);
			Driver.FindElement(LoginButton).Click();
			if (user == HtmlConsts.PlannerLogin)
				Assert.IsTrue(Driver.Url.Contains("/Planning/MediaPlans"), "Login failed");
		}

		public bool IsUserLoggedIn()
		{
			if (Driver.FindElements(LoggedInUser).Count == 1)
				return true;
			else
				return false;
		}

		public By Email { get; set; }
		public By Password { get; set; }
		public By LoginButton { get; set; }
		public By LoggedInUser { get; set; }

	}
}
