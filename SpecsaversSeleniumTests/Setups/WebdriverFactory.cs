﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace IntegrationTests
{
	public class WebDriverFactory
	{
		public static IWebDriver driver;
		private static IWebDriver _driver;

		public static IWebDriver Create(BrowserType browser)
		{
			switch (browser)
			{
				case BrowserType.GoogleChromeMobile:
					ChromeOptions emoptions = new ChromeOptions();
					emoptions.EnableMobileEmulation("iPhone 4");
					emoptions.BinaryLocation = "..\\..\\";
					driver = new ChromeDriver(emoptions);
					driver = new ChromeDriver(@"..\..\");
					driver.Manage().Window.Size = new Size(375, 667);
					break;
				case BrowserType.GoogleChrome:
					var service = ChromeDriverService.CreateDefaultService();
					//service.
					service.LogPath = "C:\\chromedriver.log";
					service.Port = 50543;
					service.HideCommandPromptWindow = false;
					service.EnableVerboseLogging = true;
					//driver = new ChromeDriver(service);
					//_driver = driver;
					//driver.Manage().Window.Maximize();	

					//var sessionId = ((ChromeDriver)driver).SessionId.ToString();

					//var sessionUrl = ((ChromeDriver)driver).Url;
					//var cd = ((ChromeDriver)driver);
					//var caps = cd.Capabilities;
					//Console.WriteLine($"Session {sessionId}  URL {sessionUrl}");

					//ReuseChromeDriver._sessionId = sessionId;
					//ReuseChromeDriver._capabilities = caps;
					//ReuseChromeDriver._sessionId = "b4f650d22ed346fc3bada2ed65edf498";

					var fileCap = @"C:\websites\delete.specsavers.uitests\SpecsaversSeleniumTests\sessioncap.txt";
					if (File.Exists(fileCap))
					{
						var sessionId = File.ReadAllText(fileCap);
						if (!string.IsNullOrEmpty(sessionId))
						{
							Response resp = new Response();
							resp.Status = WebDriverResult.Success;
							resp.SessionId = sessionId;
							ReuseChromeDriver._response = resp;
						}
					}
					
				

					var remoteDriver = new ReuseChromeDriver(service);
					//remoteDriver = new ReuseChromeDriver(service);
					
					File.WriteAllText(fileCap, ReuseChromeDriver._response.SessionId);

					//remoteDriver.Navigate().GoToUrl("https://specslink.specsavers.com/Account/LogOn?ReturnUrl=%2fAccount%2fChangeView");
					driver = remoteDriver;
					//driver.Manage().Window.Size = new Size(1280, 800);
					break;
				case BrowserType.IEDriver:
					driver = new InternetExplorerDriver();
					driver.Manage().Window.Maximize();
					//                    driver.Manage().Window.Size = new Size(1280, 800);
					break;
				case BrowserType.Firefox:
					driver = new FirefoxDriver();
					//driver.Manage().Window.Size = new Size(1280, 800);
					break;				
				default:
					driver = new ChromeDriver();
					driver.Manage().Window.Maximize();
					//driver.Manage().Window.Size = new Size(1280, 800);
					break;
			}

			//webDriver.Manage().Window.Maximize();

			return driver;
		}
		public static void Dispose()
		{
			if (_driver != null)
			{
				//string s1 = driver.GetType().ToString(); //"OpenQA.Selenium.IE.InternetExplorerDriver"
														 //                if(driver.GetType().ToString() == "OpenQA.Selenium.IE.InternetExplorerDriver")
														 //                    driver.Close();
				_driver.Quit();
			}
		}
	}


	public class ReuseChromeDriver : ChromeDriver
	{
		public static Response _response;

		public ReuseChromeDriver(ChromeDriverService service) : base (service)
		{

		}

		protected override Response Execute(string driverCommandToExecute, Dictionary<string, object> parameters)
		{

			if (driverCommandToExecute == DriverCommand.NewSession)
			{
				if (_response == null)
				{
					var respBase = base.Execute(driverCommandToExecute, parameters);
					_response = respBase;
					return respBase;
				}

				else
				{
					var commandInfoRepository = typeof(HttpCommandExecutor).GetField("commandInfoRepository", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
					var ce = this.CommandExecutor as DriverServiceCommandExecutor;
					commandInfoRepository.SetValue(ce.HttpExecutor, new W3CWireProtocolCommandInfoRepository());
					return _response;
				}
			}
			return base.Execute(driverCommandToExecute, parameters); ;
		}

	}

}