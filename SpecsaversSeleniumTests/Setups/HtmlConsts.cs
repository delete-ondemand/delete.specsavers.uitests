﻿namespace IntegrationTests
{
	public static class HtmlConsts
	{

		public const string Host = "https://specslink.specsavers.com";
		public const string TestRegionId = "23";
		public const string TestStoreId = "1811";
		public const string BudgetYear = "2019";
		public const string TestStoreUrl = Host + "/Planning/MediaPlans?RegionId=" + TestRegionId + "&StoreId=" + TestStoreId + "&Budgets.SelectedYear="+ BudgetYear + "&MediaSortOrder=Ascending";
		public const string Logout = Host + "/Account/LogOff";
		public const string PlannerQueriesTab = Host + "/Planning/PlanningQueries?DisplayActionName=PlanningQueriesList";
		public const string Password = "$ochi2014";
		public const string PlannerLogin = "planner@planner.com";
		public const string MediaAgency = "ma@ss.com";
		public const string StoreName = "JMeter Test";


		public enum MediaTypes
		{
			Ambient,
			BusPanels,
			CarParkTickets,
			CinemaTickets,
			Digital,
			DoorDrop,
			Inserts,
			Leaflets,
			Mailings,
			NationalActivity,
			Other,
			OutdoorESP,
			OutdoorUK_ROI,
			PosterPrint,
			PR,
			Press,
			Radio,
			Sponsorship,
			TillRolls,
			TV,
			Vehicular
		}

		public enum AmbientSubCategories
		{
			BalloonArch,
			BalloonModeller,
			Concierge,
			LeafletDistributor,
			LivingStatue,
			LookWalker,
			PerformanceArtist,
			Quadruped,
			ShoppingCentre,
			StiltWalker
		}
	
	}
}
