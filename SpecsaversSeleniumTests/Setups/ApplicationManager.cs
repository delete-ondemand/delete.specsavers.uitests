﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras;

namespace IntegrationTests
{
    public class ApplicationManager
    {
        public readonly int TIMEOUT_IN_SECONDS = 30;
        public static ThreadLocal<ApplicationManager> app = new ThreadLocal<ApplicationManager>();
        private readonly static ConcurrentDictionary<BrowserType, object> webDrivers = new ConcurrentDictionary<BrowserType, object>();



        public ApplicationManager(BrowserType browserType)
        {
            Driver = WebDriverFactory.Create(browserType);
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(TIMEOUT_IN_SECONDS));

        }

        public static ApplicationManager GetInstance(BrowserType browserType)
        {
            if (webDrivers.ContainsKey(browserType))
            {
                return (ApplicationManager)webDrivers[browserType];

            }
            else
            {
                app.Value = new ApplicationManager(browserType);
                webDrivers.GetOrAdd(browserType, app.Value);
                return app.Value;
            }
        }

        

        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }




    }
}