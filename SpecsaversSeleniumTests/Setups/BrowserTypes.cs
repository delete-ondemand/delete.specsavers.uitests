﻿namespace IntegrationTests
{
    public enum BrowserType
    {
        GoogleChromeMobile,
        GoogleChrome,
        Firefox,
        IEDriver,
        GridChrome,
        GridFirefox,
        GridIE10,
        GridFirefox16,
        AppiumDevice
    }
}