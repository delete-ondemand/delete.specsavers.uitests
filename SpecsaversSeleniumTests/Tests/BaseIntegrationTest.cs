﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntegrationTests
{
    [TestFixture(BrowserType.GoogleChrome)]
    //[TestFixture(BrowserType.Firefox)]
    //[TestFixture(BrowserType.GridChrome)]
    //[TestFixture(BrowserType.AppiumDevice)]
    //[TestFixture(BrowserType.GridIE10)]
    //[TestFixture(BrowserType.IEDriver)] 
    //[TestFixture(BrowserType.GridFirefox)]

    public class BaseIntegrationTest
    {
        private BrowserType _browserType;

        public BaseIntegrationTest(BrowserType browserType)
        {
            _browserType = browserType;
            App = ApplicationManager.GetInstance(_browserType);
            Driver = App.Driver;
            Wait = App.Wait;
            StoresDropdown = By.Id("StoreId_chzn");
            StoreValue = By.XPath($"//li[contains(text(), \"{HtmlConsts.StoreName}\")]");
            ApplyFilerButton = By.Id("apply-filter");
            QueryStatus = By.CssSelector("tbody td:nth-child(3)");
            PageNavigationSinglePage = By.CssSelector("span#pages-undertable>a:only-of-type");
            TableSingleRow = By.CssSelector("table.b-results>tbody tr:only-of-type");
            TableRowsCount = By.CssSelector("table.b-results>tbody tr");
        }


        public void GoToPage(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }
        public void Logout()
        {
            GoToPage(HtmlConsts.Logout);
        }
        public string GetWindowHeight()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            return js.ExecuteScript("return document.body.scrollHeight").ToString();

        }
        public By InsertionToQuery(int insertionId)
        {
            return By.CssSelector($"table.b-results>tbody tr a[href^=\"/Insertions/Details/{insertionId}\"]");
            //return By.Id("table.b-results>tbody tr a.js-show-insertions-link");
        }
        public void FilterInsertionsByStore(int insertionId)
        {
            var isDisplayed = Wait.Until(Driver => Driver.FindElement(TableRowsCount).Displayed);
            Driver.FindElement(StoresDropdown).Click();
            Driver.FindElement(StoreValue).Click();
            Driver.FindElement(ApplyFilerButton).Click();
            isDisplayed = Wait.Until(Driver => Driver.FindElement(TableSingleRow).Displayed);
            ShowAlert("Insertions are filtered");
        }

        public static int Random(int rndTo)
        {
            Random rand = new Random();
            if (rndTo == 0)
                return 0;
            return rand.Next(rndTo - 1);
        }



        protected static string[] Combine(string[] stringsA, string[] stringsB)
        {
            return stringsA.SelectMany(a => stringsB.Select(b => a + b)).ToArray();
        }

        public void WaitUntilDocumentIsReady(/*TimeSpan timeout*/)
        {
            var javaScriptExecutor = Driver as IJavaScriptExecutor;
            //var wait = new WebDriverWait(Driver, timeout);

            // Check if document is ready
            Func<IWebDriver, bool> readyCondition = Driver => (bool)javaScriptExecutor.ExecuteScript("return (document.readyState == 'complete' && jQuery.active == 0)");
            Wait.Until(readyCondition);
            ShowAlert("Document is ready");
        }

        public void ShowAlert(string alertMessage)
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript($"alert('{alertMessage}')");
            bool isRemoved = false;
            Thread.Sleep(800);
            try
            {
                IAlert alert = Driver.SwitchTo().Alert();
                isRemoved = true;
                alert.Accept();
            }
            catch (NoAlertPresentException ex)
            {
                ex.Message.ToString();
            }
        }
        public WebDriverWait Wait { get; set; }
        public IWebDriver Driver { get; set; }
        public ApplicationManager App { get; set; }
        public By StoresDropdown { get; set; }
        public By StoreValue { get; set; }
        public By ApplyFilerButton { get; set; }
        public By QueryStatus { get; set; }
        public By PageNavigationSinglePage { get; set; }
        public By TableSingleRow { get; set; }
        public By TableRowsCount { get; set; }
    }
}
