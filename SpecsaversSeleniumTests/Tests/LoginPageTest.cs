﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace IntegrationTests
{
	class LoginPageTest : LoginPage
	{
		private LoginPage _loginPage;
		public LoginPageTest(BrowserType browserType) : base(browserType)
		{
			_loginPage = new LoginPage(browserType);
		}

		[Test]
		public void LoginAsPlanner()
		{
			_loginPage.GoToPage(HtmlConsts.Host);
			Driver.FindElement(_loginPage.Email).SendKeys(HtmlConsts.PlannerLogin);
			Driver.FindElement(_loginPage.Password).SendKeys(HtmlConsts.Password);
			Driver.FindElement(_loginPage.LoginButton).Click();
			Assert.IsTrue(Driver.Url.Contains("/Planning/MediaPlans"), "Login failed");

		}

	}
}
