﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace IntegrationTests
{
	class MediaPlanTests : MediaPlanPage
	{
		private LoginPage _loginPage;
		private InsertionOverlay _insertionOverlay;
		private MediaAgencyPage _mediaAgency;
		private BrowserType _browserType;
		public MediaPlanTests(BrowserType browserType) : base(browserType)
		{
			_browserType = browserType;
			//_loginPage = new LoginPage(browserType);
		}

		[Test]
		public void CreateMedia()
		{
			_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
			GoToPage(HtmlConsts.TestStoreUrl);
			Driver.FindElement(AddMediaButton).Click();
			var isDisplayed = Wait.Until(Driver => Driver.FindElement(MediaTypeDropdown).Displayed);
			Driver.FindElement(MediaTypeDropdown).Click();
			Driver.FindElement(MediaTypeValue(1)).Click();
			Driver.FindElement(SubCategoryDropdown).Click();
			Driver.FindElement(SubCategoryValue(1)).Click();
			Driver.FindElement(QuantityInput).SendKeys("4");
			Driver.FindElement(SaveMediaButton).Click();
			Thread.Sleep(5000);
			Driver.FindElement(RemoveMediabyId(GetLastMediaId())).Click();


		}
		[Test]
		public void AddNewInsertionToMediaPlan()
		{
			GoToPage(HtmlConsts.TestStoreUrl);
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
				GoToPage(HtmlConsts.TestStoreUrl);
			}
			//Create Press - Press media, 16 - 6
			var mediaId = AddNewMedia(16, 6);
			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("True", "False", "True");
			_insertionOverlay.CreateNewInsertion();
		}

		[Test]
		public void CreateInsertionWithDispatchStatus()
		{
			GoToPage(HtmlConsts.TestStoreUrl);
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
				GoToPage(HtmlConsts.TestStoreUrl);
			}
			//Create Press - Press media, 16 - 6
			var mediaId = AddNewMedia(16, 6);

			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("False", "False", "False");
			_insertionOverlay.CreateNewInsertion();
			_insertionOverlay = OpenInsertionDetails(mediaId);
			Assert.AreEqual(Driver.FindElement(_insertionOverlay.InsertionStatus).Text, "Dispatched");
			_insertionOverlay.RemoveInsertion();
			//RemoveMedia(mediaId);
			//Thread.Sleep(5000);

		}

		[Test]
		public void CreateSubmittedInsertion()
		{
			GoToPage(HtmlConsts.TestStoreUrl);
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
				GoToPage(HtmlConsts.TestStoreUrl);
			}

			var mediaId = GetLastMediaId();
			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("False", "True", "False");
			_insertionOverlay.CreateNewInsertion();
			_insertionOverlay = OpenInsertionDetails(mediaId);
			Assert.AreEqual(Driver.FindElement(_insertionOverlay.InsertionStatus).Text, "Submitted");
			_insertionOverlay.CancelInsertion();
		}

		[Test]
		public void CreateBriefedInsertion()
		{
			GoToPage(HtmlConsts.TestStoreUrl);
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
				GoToPage(HtmlConsts.TestStoreUrl);
			}
			var mediaId = GetLastMediaId();
			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("False", "False", "True");
			_insertionOverlay.CreateNewInsertion();
			_insertionOverlay = OpenInsertionDetails(mediaId);
			Assert.AreEqual(Driver.FindElement(_insertionOverlay.InsertionStatus).Text, "Briefed");
			_insertionOverlay.RemoveInsertion();
			Thread.Sleep(5000);
		}
		[Test]
		public void CreateBookedInsertion()
		{
			GoToPage(HtmlConsts.TestStoreUrl);
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
				GoToPage(HtmlConsts.TestStoreUrl);
			}
			var mediaId = GetLastMediaId();
			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("True", "False", "False");
			_insertionOverlay.CreateNewInsertion();
			_insertionOverlay = OpenInsertionDetails(mediaId);
			Assert.AreEqual(Driver.FindElement(_insertionOverlay.InsertionStatus).Text, "Booked");
			_insertionOverlay.RemoveInsertion();
			Thread.Sleep(5000);
		}

		[Test]
		public void CreateBriefedInsertionAndQueryByAgency()
		{
			//_loginPage = new LoginPage(BrowserType.GoogleChrome);
			_loginPage = new LoginPage(_browserType);
			_mediaAgency = new MediaAgencyPage(_browserType);

			Logout();
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);
			}
			GoToPage(HtmlConsts.TestStoreUrl);
			//Add Press - Press media
			var mediaId = AddNewMedia(16, 6);

			_insertionOverlay = AddInsertion(mediaId);
			_insertionOverlay.SelectCampaignCode();
			_insertionOverlay.Set_Production_StoreApproval_AgencyBooking("False", "False", "True");

			_insertionOverlay.CreateNewInsertion();
			int insertionId = GetInsertionId(mediaId);
			_insertionOverlay = OpenInsertionDetails(mediaId);
			Assert.AreEqual(Driver.FindElement(_insertionOverlay.InsertionStatus).Text, "Briefed");

			Logout();
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.MediaAgency);
			}
			_mediaAgency = new MediaAgencyPage(_browserType);
			WaitUntilDocumentIsReady(); //shows alert


			FilterInsertionsByStore(insertionId); // shows alert
			_insertionOverlay = _mediaAgency.OpenInsertionOverlay(insertionId);
			_insertionOverlay.QueryInsertionByMediaAgency(insertionId); // shows alert
			_mediaAgency.ProceedToQueriesTab(); // shows alert

			var status = _mediaAgency.CheckAgencyQueryStatus(insertionId);
			Assert.AreEqual(status, "Awaiting planner response");
			Logout();
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);

			}
			GoToPage(HtmlConsts.PlannerQueriesTab);
			WaitUntilDocumentIsReady();
			FilterInsertionsByStore(insertionId);// shows alert
			status = _mediaAgency.CheckPlannerQueryStatus(insertionId);
			Assert.AreEqual(status, "Agency query");
			_insertionOverlay = _mediaAgency.OpenInsertionOverlay(insertionId);
			_insertionOverlay.PlannerReplyQuery(insertionId); // shows alert
			Assert.AreEqual(Driver.FindElements(InsertionToQuery(insertionId)).Count, 0);
			Logout();
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.MediaAgency);
			}
			_mediaAgency.ProceedToQueriesTab();// shows alert
			FilterInsertionsByStore(insertionId);// shows alert
			status = _mediaAgency.CheckAgencyQueryStatus(insertionId);
			Assert.AreEqual(status, "Response received");
			_insertionOverlay = _mediaAgency.OpenInsertionOverlay(insertionId);
			_insertionOverlay.MediaAgencySetInsertionAsResolved(insertionId);
			Logout();
			if (!_loginPage.IsUserLoggedIn())
			{
				_loginPage.LoginAsUser(HtmlConsts.PlannerLogin);

			}
			GoToPage(HtmlConsts.TestStoreUrl);
			_insertionOverlay = OpenInsertionDetails(mediaId);
			_insertionOverlay.RemoveInsertion();

		}
	}
}
